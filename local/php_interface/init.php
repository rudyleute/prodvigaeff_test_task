<?php

\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\WorkOrganizer\Classes\DatabaseWork' => '/local/php_interface/classes/database_work.php',
    '\WorkOrganizer\Classes\LatenessTable' => '/local/php_interface/classes/lateness.php',
    '\WorkOrganizer\Classes\ProfileTable' => '/local/php_interface/classes/profile.php',
    '\WorkOrganizer\Classes\WorkDayPauseTable' => '/local/php_interface/classes/workday_pause.php',
    '\WorkOrganizer\Classes\WorkDayTable' => '/local/php_interface/classes/workday.php',
));