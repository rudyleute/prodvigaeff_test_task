<?php

namespace WorkOrganizer\Classes;

use Bitrix\Main;

class WorkDayPauseTable extends DatabaseWork
{
    public static function getTableName()
    {
        return 'workday_pause';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID профиля",
            ),
            'WORKDAY_ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => 'ID профиля',
            ),
            'WORKDAY' => array(
                'data_type' => '\WorkOrganizer\Classes\WorkDayTable',
                'reference' => array('=this.WORKDAY_ID' => 'ref.ID'),
            ),
            'DATE_START' => array(
                'data_type' => 'datetime',
                'required' => false,
                'title' => "Время начала дня",
            ),
            'DATE_STOP' => array(
                'data_type' => 'datetime',
                'required' => false,
                'title' => "Время окончания дня",
            ),
        );
    }

    protected static function createDatabase()
    {
        $query = "CREATE TABLE IF NOT EXISTS `" . self::getTableName() . "` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `workday_id` int NOT NULL,
              `date_start` datetime NULL,
              `date_stop` datetime NULL,  
              FOREIGN KEY (`workday_id`) REFERENCES " . WorkDayTable::getTableName() . "(`id`)
        )";

        global $DB;
        $DB->Query($query);
    }

    protected static function doesUnfinishedPauseExist($workdayId)
    {
        $data = self::getList(array(
            "filter" => array(
                "WORKDAY_ID" => $workdayId,
                "=DATE_STOP" => false,
            ),
        ))->fetchAll();

        return array(
            "exists" => !empty($data),
            "dataId" => !empty($data) ? $data[0]["ID"] : null
        );
    }

    public static function startPause($workdayId)
    {
        $errors= [];

        $result = WorkDayTable::doesUnfinishedDayExist($workdayId);

        if ($result["exists"]) $errors[] = "There is no open workdays with such an Id or working day was already closed";
        
        if (empty($errors)) {

            $result = self::doesUnfinishedPauseExist($workdayId);

            if ($result["exists"]) {
                $errors[] = "Workday with such an id does not exist or you have an unfinished pause";
            }

            if (empty($errors)) {
                $result = WorkDayTable::doesUnfinishedDayExist($workdayId);

                if (!$result["exists"])
                    $result = self::add(array(
                        'WORKDAY_ID' => $workdayId,
                        'DATE_START' => new Main\Type\DateTime(gmdate('Y-m-d H:i:s'),'Y-m-d H:i:s')
                    ));

                if (!$result->isSuccess()) {
                    $errors = $result->getErrorMessages();
                }
            }
        }

        return self::convertedOutput(empty($errors), $errors);
    }

    public static function finishPause($workdayId)
    {
        $errors = [];

        $result = self::doesUnfinishedPauseExist($workdayId);

        if (!$result["exists"]) {
            $errors[] = "Such a working day was not found or the pause was already closed";
        }

        if (empty($errors)) {
            $result = self::update($result["dataId"], array(
                'DATE_STOP' => new Main\Type\DateTime(gmdate('Y-m-d H:i:s'),'Y-m-d H:i:s')
            ));

            if (!$result->isSuccess()) {
                $errors = $result->getErrorMessages();
            }
        }
        return self::convertedOutput(empty($errors), $errors);
    }
}
