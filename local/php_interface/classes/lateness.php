<?php

namespace WorkOrganizer\Classes;

use Bitrix\Main;

class LatenessTable extends DatabaseWork
{
    public static function getTableName()
    {
        return 'lateness';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID записи",
            ),
            'PROFILE_ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => 'ID профиля',
            ),
            'PROFILE' => array(
                'data_type' => '\WorkOrganizer\Classes\ProfileTable',
                'reference' => array('=this.PROFILE_ID' => 'ref.ID'),
            ),
            'DATE' => array(
                'data_type' => 'date',
                'required' => true,
                'title' => "Дата опоздания",
            ),
        );
    }

    protected static function createDatabase()
    {
        $query = "CREATE TABLE IF NOT EXISTS `" . self::getTableName() . "` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,   
              `profile_id` int NOT NULL,       
              `date` DATE NULL,
              FOREIGN KEY (`profile_id`) REFERENCES " . ProfileTable::getTableName() . "(`id`)       
        )";

        global $DB;
        $DB->Query($query);
    }

    protected static function wasAlreadyWrittenToday($profileId, $shift)
    {
        $date = new Main\Type\Date();
        $data = self::getList(array(
            "filter" => array(
                "PROFILE_ID" => $profileId,
                "=DATE" => $date->add($shift . " days")
            )
        ))->fetchAll();

        return !empty($data);
    }

    public static function writeLateness($profileId, $offset, $workStartsHour)
    {
        $errors = [];

        $shift = WorkDayTable::getDateShift($offset, $workStartsHour)["shift"];

        if (self::wasAlreadyWrittenToday($profileId, $shift)) {
            $errors[] = "Profile with such id was not found or profile was already set as late for today";
        }

        $date = new Main\Type\Date();

        if (empty($errors)) {
            $result = self::add(array(
                "PROFILE_ID" => $profileId,
                "DATE" => $date->add($shift . " days")
            ));

            if (!$result->isSuccess()) {
                $errors = $result->getErrorMessages();
            }
        }

        return self::convertedOutput(empty($errors), $errors);
    }

    public static function addAllLateness($workStartsHour = 9) {
        $profileIds = WorkDayTable::getLateProfilesId($workStartsHour);

        foreach ($profileIds as $oneId) {
            self::writeLateness($oneId["ID"], $oneId["OFFSET"], $workStartsHour);
        }
    }
}
