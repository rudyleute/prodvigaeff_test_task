<?php


namespace WorkOrganizer\Classes;

use Bitrix\Main;

class DatabaseWork extends Main\Entity\DataManager
{
    public static function validateLength($length = 255)
    {
        return array(
            new Main\Entity\Validator\Length(null, $length),
        );
    }

    public static function createAllDatabases() {
        ProfileTable::createDatabase();
        WorkDayTable::createDatabase();
        WorkDayPauseTable::createDatabase();
        LatenessTable::createDatabase();
    }

    protected static function convertedOutput($success, $error) {
        return array(
            "success" => $success,
            "errors" => $error
        );
    }
}