<?php

namespace WorkOrganizer\Classes;

use Bitrix\Main;

class ProfileTable extends DatabaseWork
{
    public static function getTableName()
    {
        return 'profile';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID профиля",
            ),
            'LOGIN' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateLength'),
                'title' => "Логин",
            ),
            'NAME' => array(
                'data_type' => 'string',
                'required' => false,
                'validation' => array(__CLASS__, 'validateLength'),
                'title' => "Имя профиля",
            ),
            'LAST_NAME' => array(
                'data_type' => 'string',
                'required' => false,
                'validation' => array(__CLASS__, 'validateLength'),
                'title' => "Фамилия профиля",
            ),
            'OFFSET' => array(
                'data_type' => 'string',
                'required' => false,
                'validation' => function () {
                    return array(
                        new Main\Entity\Validator\Length(null, 10),
                        new Main\Entity\Validator\RegExp('/^[-+][\d]{4,}/'),
                    );
                },
                'title' => "Смещение часового пояса",
            ),
        );
    }

    protected static function createDatabase()
    {
        $query = "CREATE TABLE IF NOT EXISTS `" . self::getTableName() . "`(
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `login` varchar(255) NOT NULL,
              `name` varchar(255) NULL,
              `last_name` varchar(255) NULL,
              `offset` varchar(10) NULL
        );";

        global $DB;
        $DB->Query($query);
    }

    public static function profilesOffsetMeetHour($closeWorkdayHour = 24)
    {
        $timezoneForClosing = $closeWorkdayHour - intval(gmdate('H'));

        if ($timezoneForClosing > 12) $timezoneForClosing -= 24;

        $timezoneForClosingStr = sprintf("%0+3d", $timezoneForClosing);

        $data = self::getList(array(
            "filter" => array(
                "%=OFFSET" => $timezoneForClosingStr . "%"
            ),
            "select" => array("ID")
        ))->fetchAll();

        $profileIds = [];
        foreach ($data as $oneItem) {
            $profileIds[] = $oneItem["ID"];
        }

        return $profileIds;
    }

    public static function doesLoginExist($login)
    {
        $data = self::getList(array(
            "filter" => array(
                "LOGIN" => $login
            )
        ))->fetchAll();

        return !empty($data);
    }

    public static function addProfile($data)
    {
        $errors = [];

        if (empty($data["login"])) $errors[] = "Login field must be filled in";

        if (empty($errors)) {
            if (self::doesLoginExist($data["login"])) $errors[] = "Such login already exists";

            if (empty($errors)) {
                $result = self::add(array(
                    "LOGIN" => $data["login"],
                    "NAME" => !empty($data["name"]) ? $data["name"] : null,
                    "LAST_NAME" => !empty($data["last_name"]) ? $data["last_name"] : null,
                    "OFFSET" => !empty($data["offset"]) ? $data["offset"] : null
                ));

                if (!$result->isSuccess()) $errors = $result->getErrorMessages();
            }
        }

        return self::convertedOutput(empty($errors), $errors);
    }
}
