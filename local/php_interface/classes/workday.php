<?php

namespace WorkOrganizer\Classes;

use Bitrix\Main;

class WorkDayTable extends DatabaseWork
{
    public static function getTableName()
    {
        return 'workday';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID профиля",
            ),
            'PROFILE_ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => 'ID профиля',
            ),
            'PROFILE' => array(
                'data_type' => '\WorkOrganizer\Classes\ProfileTable',
                'reference' => array('=this.PROFILE_ID' => 'ref.ID'),
            ),
            'DATE_START' => array(
                'data_type' => 'datetime',
                'required' => false,
                'title' => "Время начала дня",
            ),
            'DATE_STOP' => array(
                'data_type' => 'datetime',
                'required' => false,
                'title' => "Время окончания дня",
            ),
        );
    }

    protected static function createDatabase()
    {
        $query = "CREATE TABLE IF NOT EXISTS `" . self::getTableName() . "`(
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `profile_id` int NOT NULL,  
              `date_start` datetime NULL,
              `date_stop` datetime NULL,
              FOREIGN KEY (`profile_id`) REFERENCES " . ProfileTable::getTableName() . "(`id`)   
        )";

        global $DB;
        $DB->Query($query);
    }

    public static function doesUnfinishedDayExist($profileId)
    {
        $data = self::getList(array(
            "filter" => array(
                "PROFILE_ID" => $profileId,
                "=DATE_STOP" => false,
            ),
        ))->fetchAll();

        return array(
            "exists" => !empty($data),
            "dataId" => !empty($data) ? $data[0]["ID"] : null
        );
    }

    public static function startDay($profileId)
    {
        $errors = [];

        $result = self::doesUnfinishedDayExist($profileId);

        if ($result["exists"]) {
            $errors[] = "Profile with such an id does not exist or you have an unfinished day";
        }

        if (empty($errors)) {
            $result = self::add(array(
                'PROFILE_ID' => $profileId,
                'DATE_START' => new Main\Type\DateTime(gmdate('Y-m-d H:i:s'),'Y-m-d H:i:s')
            ));

            if (!$result->isSuccess()) {
                $errors = $result->getErrorMessages();
            }
        }

        if (!empty($errors)) {
            return self::convertedOutput(empty($errors), $errors);
        }

        return array_merge(
            self::convertedOutput(empty($errors), $errors),
            array("result" => ["workday_id" => $result->getId()])
        );
    }

    public static function updateDay($data){
        $errors = [];
        $result = self::update($data["dataId"], $data["fields"]);

        if (!$result->isSuccess()) {
            $errors = $result->getErrorMessages();
        }

        return self::convertedOutput(empty($errors), $errors);
    }

    public static function finishDay($profileId)
    {
        $errors = [];

        $result = self::doesUnfinishedDayExist($profileId);

        if (!$result["exists"]) {
            $errors[] = "Such a working day was not found or the day was already closed";
        }

        if (empty($errors)) {

            return self::updateDay(array(
                "dataId" => $result["dataId"],
                "fields" => array(
                    'DATE_STOP' => new Main\Type\DateTime(gmdate('Y-m-d H:i:s'),'Y-m-d H:i:s')
                )
            ));
        }
        return self::convertedOutput(empty($errors), $errors);
    }

    public static function closeAllDays($dayStartsHour = 24)
    {
        $profileIds = ProfileTable::profilesOffsetMeetHour($dayStartsHour);

        $data = self::getList(array(
            "filter" => array(
                "PROFILE_ID" => $profileIds,
                "DATE_STOP" => false
            ),
            "select" => array("ID")
        ))->fetchAll();

        foreach ($data as $oneItem) {
            self::updateDay(array(
                "dataId" => $oneItem["ID"],
                "fields" => array(
                    'DATE_STOP' => new Main\Type\DateTime(gmdate('Y-m-d H:i:s'),'Y-m-d H:i:s')
                )
            ), false);
        }
    }

    public static function getDateShift($offset, $workStartHour) {
        $offset = intval(substr($offset, 0, 3));
        $workStartsAtGreenwichHour = $workStartHour - $offset;
        $dayShift = 0;

        if ($workStartsAtGreenwichHour < 0) {
            $workStartsAtGreenwichHour = 24 + $workStartsAtGreenwichHour;
            $dayShift--;
        }
        elseif ($workStartsAtGreenwichHour >= 24) {
            $workStartsAtGreenwichHour = 24 - $workStartsAtGreenwichHour;
            $dayShift++;
        }

        return array(
            "shift" => $dayShift,
            "greenwichStartTime" => $workStartsAtGreenwichHour
        );
    }

    public static function isProfileLate($offset, $workStartHour, $startDatetime) {
        $shiftData = self::getDateShift($offset, $workStartHour);

        $dayShift = $shiftData["shift"];

        $curStartDate = gmdate('Y-m-d', strtotime($dayShift . " days"));

        $workStartTimeStr = $curStartDate . ' ' . sprintf("%02d", $shiftData["greenwichStartTime"]) . ":00:00";

        return strtotime($startDatetime->toString()) > strtotime($workStartTimeStr);
    }

    public static function getLateProfilesId($workStartsHour = 9)
    {
        //Получить текущий offset с провекой на 9-ку
        //вытащить всех пользователей по этому оффсету
        //проверить на время и наличие в базе за сегодня
        //посмотреть, как сравнивать время datetime

        $data = self::getList(array(
            "filter" => array(
                ">=DATE_START" => gmdate("Y-m-d H:i:s", strtotime("-1 days"))
            ),
            "select" => array("ID", "PROFILE_ID", "DATE_START")
        ))->fetchAll();

        $profileIds = [];
        $profileIdsStartTime = [];
        foreach ($data as $oneItem) {
            $profileIds[] = $oneItem["PROFILE_ID"];
            $profileIdsStartTime[$oneItem["PROFILE_ID"]] = array(
                "DATE_START" => $oneItem["DATE_START"],
                "WORKDAY_ID" => $oneItem["ID"]
            );
        }

        $data = ProfileTable::getList(array(
            "filter" => array(
                "ID" => $profileIds
            ),
            "select" => array("ID", "OFFSET")
        ))->fetchAll();

        $profileIds = [];
        foreach ($data as $oneProfile) {
            if (self::isProfileLate($oneProfile["OFFSET"], $workStartsHour, $profileIdsStartTime[$oneProfile["ID"]]["DATE_START"])) {
                $profileIds[] = array("ID" => $oneProfile["ID"], "OFFSET" => $oneProfile["OFFSET"]);
            }
        }
        return $profileIds;
    }
}
