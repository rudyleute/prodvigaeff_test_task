<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

\WorkOrganizer\Classes\WorkDayTable::closeAllDays();
\WorkOrganizer\Classes\LatenessTable::addAllLateness();